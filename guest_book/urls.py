from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^guestbook/', include('gbook.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
