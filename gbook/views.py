from datetime import datetime

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .models import Book, Note
from .forms import BookForm, NoteForm


@login_required
def library(request):
    """ list of books
    """
    return render(request, 'library.html', {
        'all_books': Book.objects.all(),
    })


@login_required
def book(request, slug):
    """ display all notes and comments
    for selected book (slug)
    """
    return render(request, 'book.html', {
        'book': Book.objects.get(slug=slug),
    })


@login_required
def create_book(request):
    """ create new book
    """
    bookform = BookForm()
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            new_book = Book(
                slug=form.cleaned_data['slug'],
                name=form.cleaned_data['name'],
                premoderation=form.cleaned_data['premoderation'],
                owner=request.user,
                date=datetime.now()
            )
            new_book.save()
            return redirect('/guestbook/library/')
    return render(request, 'create_book.html', {'BookForm': bookform})


@login_required
def add_note(request, slug):
    """ add note for selected book (slug)
    """
    noteform = NoteForm()
    book_obj = Book.objects.get(slug=slug)
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            new_note = Note(
                name=form.cleaned_data['name'],
                text=form.cleaned_data['text'],
                book=book_obj,
                checked='good' if book.premoderation == False else 'unknown',
                writer=request.user,
                date=datetime.now()
            )
            new_note.save()
            return redirect('/guestbook/book/%s' % slug)
    return render(request, 'add_note.html',
                  {'NoteForm': noteform, 'slug': slug, 'book': book})


@login_required
def premoderate(request):
    """ premoderate all notes in books of request.user, if book.premoderate == True
    """
    notes = Note.objects.filter(book__owner=request.user,
                                book__premoderation=True,
                                checked='unknown',
                               ).all()
    if request.method == 'POST':
        if request.POST['note_id'] and request.POST['checked'] != 'unknown':
            Note.objects.filter(id=request.POST['note_id'])\
                        .update(checked=request.POST['checked'])
    return render(request, 'premoderate.html', {'notes': notes,})
