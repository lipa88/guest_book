from django.db import models
from django.conf import settings
from datetime import datetime


MODERATION_STATUS = (
    ('good', 'good'),
    ('bad', 'bad'),
    ('unknown', 'unknown'),
)


class Book(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)
    slug = models.SlugField(unique=True)
    premoderation = models.BooleanField(default=False)
    date = models.DateTimeField(default=datetime.now)
    def __str__(self):
        return self.name
    class Meta:
        order_with_respect_to = 'owner'
        ordering = ['date', 'name']


class Note(models.Model):
    name = models.CharField(max_length=200, blank=True)
    text = models.TextField()
    checked = models.CharField(max_length=8,
                               choices=MODERATION_STATUS,
                               default='unknown')
    writer = models.ForeignKey(settings.AUTH_USER_MODEL)
    book = models.ForeignKey(Book)
    date = models.DateTimeField(default=datetime.now)
    def __str__(self):
        return self.name
    class Meta:
        order_with_respect_to = 'book'
        ordering = ['date']


