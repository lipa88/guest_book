# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gbook', '0002_auto_20150514_1205'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='note',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='writer',
        ),
        migrations.DeleteModel(
            name='Comment',
        ),
    ]
