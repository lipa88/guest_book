# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('gbook', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='book',
            options={'ordering': ['date', 'name']},
        ),
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['date']},
        ),
        migrations.AlterModelOptions(
            name='note',
            options={'ordering': ['date']},
        ),
        migrations.AddField(
            model_name='book',
            name='date',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='comment',
            name='date',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='note',
            name='date',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AlterField(
            model_name='book',
            name='slug',
            field=models.SlugField(unique=True),
        ),
        migrations.AlterOrderWithRespectTo(
            name='book',
            order_with_respect_to='owner',
        ),
        migrations.AlterOrderWithRespectTo(
            name='note',
            order_with_respect_to='book',
        ),
    ]
