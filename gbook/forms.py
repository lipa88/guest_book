from gbook.models import Book, Note
from django.forms import ModelForm


class BookForm(ModelForm):
    " Create book "
    class Meta:
        model = Book
        exclude = ('owner', 'date')


class NoteForm(ModelForm):
    " Add note "
    class Meta:
        model = Note
        exclude = ('checked', 'writer', 'date', 'book')
