from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(

    '',

    url(r'^library/$', 'gbook.views.library', name='library'),
    url(r'^book/(.*)/$', 'gbook.views.book', name='book'),
    url(r'^createbook/$', 'gbook.views.create_book', name='createbook'),
    url(r'^addnote/(.*)/$', 'gbook.views.add_note', name='addnote'),
    url(r'^premoderate/$', 'gbook.views.premoderate', name='premoderate'),

    url(r'^comments/', include('django_comments.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout',
        {'next_page': '/guestbook/login/'}, name='logout'),
)
