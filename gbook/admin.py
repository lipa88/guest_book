from django.contrib import admin
from .models import Note, Book

admin.site.register(Book)
admin.site.register(Note)
