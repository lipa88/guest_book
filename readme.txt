Install the requirements:

    pip install 'django>=1.8' django-contrib-comments

Now you can run the application by using runserver command:

    python manage.py runserver
